from django.shortcuts import render
from ipl_app.models import Matches, Deliveries
from django.db.models import Count, Avg, Sum
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT

from django.views.decorators.cache import cache_page


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)




@cache_page(CACHE_TTL)
def no_of_matches_played_per_year(request):
    season_list = []
    match_count_list = []
    matches_data = Matches.objects.values('season').annotate(Count('season')).order_by('season')
    for match_data in matches_data:
        match_count_list.append(match_data['season__count'])
        season_list.append(match_data['season'])
    plot_dict = {'season': season_list, 'count': match_count_list}
    return render(request, 'ipl_app/qone.html', {'plot_data': plot_dict})

@cache_page(CACHE_TTL)
def matches_won_of_all_teams(request):
    matches_data = Matches.objects.values('season', 'winner').annotate(Count('winner')).order_by('season')
    winners_per_season = {}
    for rows in matches_data:
       if rows['winner'] != '':
           if rows['winner'] not in winners_per_season.keys():
               winners_per_season[rows['winner']] = {rows['season']:rows['winner__count']}
           else:
               winners_per_season[rows['winner']][rows['season']] = rows['winner__count']

    season_list = [season for season in range(2008, 2018)]
    for team, wins in winners_per_season.items():
        for season in season_list:
            if season not in wins.keys():
                wins[season] = 0

        sorted_wins = sorted(wins)
        sorted_win_dict = {}
        for win in sorted_wins:
            sorted_win_dict[win] = wins[win]
        winners_per_season[team] = sorted_win_dict
        return render(request, 'ipl_app/qtwo.html', {'winners_per_season': winners_per_season})

@cache_page(CACHE_TTL)
def extra_run_conceded_per_team(request):
    team_list = []
    extra_runs_list = []
    matches_data = Deliveries.objects.values('bowling_team').annotate(Sum('extra_runs')).filter(match_id__in = Matches.objects.filter(season = '2016'))
    for match_data in matches_data:
        extra_runs_list.append(match_data['extra_runs__sum'])
        team_list.append(str(match_data['bowling_team']))
    plot_dict = {'teams': team_list, 'extra_runs': extra_runs_list}
    return render(request, 'ipl_app/qthree.html', {'plot_data': plot_dict})

@cache_page(CACHE_TTL)
def top_economical_bowlers(request):
    bowler_list = []
    extra_runs_list = []
    matches_data = Deliveries.objects.values('bowler').annotate(economy = Avg('total_runs') * 6).filter(match_id__in=Matches.objects.filter(season='2015')).order_by('economy')[:10]
    for match_data in matches_data:
        extra_runs_list.append(match_data['economy'])
        bowler_list.append(str(match_data['bowler']))
    print(bowler_list)
    plot_dict = {'bowlers': bowler_list, 'economy': extra_runs_list}
    return render(request, 'ipl_app/qfour.html', {'plot_data': plot_dict})

@cache_page(CACHE_TTL)
def total_no_of_sixes_of_all_seasons(request):
    sixes_list = []
    season_list = []
    total_sixes_per_season = Deliveries.objects.only('match_id').filter(batsman_runs=6).select_related('id').values('match_id__season').annotate(Count('match_id__season')).order_by('match_id__season')
    for sixes_data in total_sixes_per_season:
        sixes_list.append(sixes_data['match_id__season__count'])
        season_list.append(sixes_data['match_id__season'])
    plot_dict = {'season': season_list, 'sixes': sixes_list}
    return render(request, 'ipl_app/qfive.html', {'plot_data': plot_dict})
