from django.core.management.base import BaseCommand
from django.db import transaction
from ipl_app.models import Matches, Deliveries
import csv


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('csv_path', type=str)
        parser.add_argument('model_name', type=str)
    
    def handle(self, *args, **options):
        csv_path = options['csv_path']
        model_name = options['model_name']
        import_data(csv_path, model_name)
        

def import_data(csv_path, model_name):
    with open(csv_path, 'r') as file_handle:
            imported_data = csv.DictReader(file_handle)
            # imported_data = csv.reader(file_handle)
            if model_name == 'Matches':
                insert_matches(imported_data)
            elif model_name == 'Deliveries':
                insert_deliveries(imported_data)

@transaction.atomic
def insert_matches(imported_data):
    # bulk_data = []
    for data in imported_data:
        # bulk_data.append(Matches(*list(data.values())))
        model_obj = Matches()
        for key, value in data.items():
            # if key != 'id':
            setattr(model_obj, key, value)
        model_obj.save()
    # Matches.objects.bulk_create(bulk_data)

@transaction.atomic
def insert_deliveries(imported_data):
    # bulk_data = []
    # i = 1
    for data in imported_data:
        # row_list = list(data.values())
        # row_list.insert(0,i)
        # i += 1
        # bulk_data.append(Deliveries(*row_list))
        model_obj = Deliveries()
        for key, value in data.items():
            if key == 'match_id':
                value = Matches.objects.get(pk=value)
            setattr(model_obj, key, value)
        model_obj.save()
    # Deliveries.objects.bulk_create(bulk_data)
            