from django.urls import path

from . import views

app_name = 'ipl_app'
urlpatterns = [
    path('q1/', views.no_of_matches_played_per_year, name='qone'),
    path('q2/', views.matches_won_of_all_teams, name='qtwo'),
    path('q3/', views.extra_run_conceded_per_team, name='qthree'),
    path('q4/', views.top_economical_bowlers, name='qfour'),
    path('q5/', views.total_no_of_sixes_of_all_seasons, name='qfive'),

]